package br.com.calculator.api.testcalculator.exceptions;

import br.com.calculator.api.testcalculator.exceptions.enums.QuantityEatExceptionEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class QuantityEatException extends CalculatorApiException {
  private static final long serialVersionUID = -4589179341768493322L;
  private final QuantityEatExceptionEnum error;

  public QuantityEatException(QuantityEatExceptionEnum error) {
    super(error.getMessage());
    this.error = error;
  }
}