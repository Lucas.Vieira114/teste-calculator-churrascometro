package br.com.calculator.api.testcalculator.facades.impl;

import br.com.calculator.api.testcalculator.dtos.responses.PersonEatsResponseDTO;
import br.com.calculator.api.testcalculator.entities.PersonEatsEntity;
import br.com.calculator.api.testcalculator.facades.PersonEatsFacade;
import br.com.calculator.api.testcalculator.services.PersonEatsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonEatsFacadeImpl implements PersonEatsFacade {

   @Autowired
   private PersonEatsService personEatsService;

   @Autowired
   private ModelMapper modelMapper;

   @Override
   public List<PersonEatsResponseDTO> findAll() {
     List<PersonEatsResponseDTO> personEatsResponseDTOS = new ArrayList<>();
     for (PersonEatsEntity personEatsEntity : personEatsService.findAll()) {
       personEatsResponseDTOS.add(convertPersonEatsEntityToPersonEatsResponseDto(personEatsEntity));
     }
     return personEatsResponseDTOS;
   }

  @Override
  public PersonEatsResponseDTO findById(Long id) {
    return convertPersonEatsEntityToPersonEatsResponseDto(personEatsService.findById(id));
  }

  private PersonEatsResponseDTO convertPersonEatsEntityToPersonEatsResponseDto(PersonEatsEntity personEatsEntity) {
    return modelMapper.map(personEatsEntity, PersonEatsResponseDTO.class);
  }

}