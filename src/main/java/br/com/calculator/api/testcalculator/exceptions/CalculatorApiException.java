package br.com.calculator.api.testcalculator.exceptions;

public class CalculatorApiException extends RuntimeException {

    private static final long serialVersionsUID = 1L;

    public CalculatorApiException(String message) {
      super(message);
    }
}
