package br.com.calculator.api.testcalculator.exceptions;

import br.com.calculator.api.testcalculator.exceptions.models.ErrorObject;
import br.com.calculator.api.testcalculator.exceptions.models.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

  private final String ERROR_MESSAGE = "Request has invalid or not informed fields!";

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                HttpHeaders headers,
                                                                HttpStatus status,
                                                                WebRequest request) {
    List<ErrorObject> errors = getErros(ex);
    ErrorResponse errorResponse = getErrorResponse(ex, status, errors);
    log.error("{Data entry error!}", errorResponse);
    return new ResponseEntity<>(errorResponse, status);
  }

  private List<ErrorObject> getErros(MethodArgumentNotValidException ex) {
    return ex.getBindingResult().getFieldErrors().stream().map(
        error -> new ErrorObject(
            error.getDefaultMessage(),
            error.getField(),
            error.getRejectedValue()
        )).collect(Collectors.toList());
  }

  private ErrorResponse getErrorResponse(MethodArgumentNotValidException ex, HttpStatus status, List<ErrorObject> errors) {
    return new ErrorResponse(
        Instant.now().toEpochMilli(),
        status.getReasonPhrase(),
        status.value(),
        ERROR_MESSAGE,
        errors);
  }

  //Foreing Key
  @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
  public ResponseEntity<ErrorResponse> sqlIntegrityConstrainViolationException(Exception e) {
    log.error("Incorrect foreing key!");
    return new ResponseEntity<>(new ErrorResponse(
        Instant.now().toEpochMilli(),
        HttpStatus.BAD_REQUEST.getReasonPhrase(),
        HttpStatus.BAD_REQUEST.value(),
        "Incorrect foreing key!",
        null), HttpStatus.BAD_REQUEST);
  }

  //Id not found
  @ExceptionHandler(EmptyResultDataAccessException.class)
  public ResponseEntity<ErrorResponse> emptyResultDataAccessException(Exception e) {
    log.error("Id not found!");
    return new ResponseEntity<>(new ErrorResponse(Instant.now().toEpochMilli(),
        HttpStatus.BAD_REQUEST.getReasonPhrase(),
        HttpStatus.BAD_REQUEST.value(),
        "Id not found!",
        null), HttpStatus.BAD_REQUEST);
  }

  //PersonEats
  @ResponseBody
  @ExceptionHandler(PersonEatsException.class)
  ResponseEntity<ErrorResponse> handlerPersonEatsException(PersonEatsException e) {
    return ResponseEntity.status(e.getError().getStatusCode())
        .body((new ErrorResponse(Instant.now().toEpochMilli(),
            e.getError().getCode(),
            e.getError().getStatusCode(),
            e.getMessage(), new ArrayList<>())));
  }

  //PercentageBarbecue
  @ResponseBody
  @ExceptionHandler(PercentageBarbecueException.class)
  ResponseEntity<ErrorResponse> handlerPercentageBarbecueException(PercentageBarbecueException e) {
    return ResponseEntity.status(e.getError().getStatusCode())
        .body((new ErrorResponse(Instant.now().toEpochMilli(),
            e.getError().getCode(),
            e.getError().getStatusCode(),
            e.getMessage(), new ArrayList<>())));
  }

  //PercentageBarbecue
  @ResponseBody
  @ExceptionHandler(QuantityEatException.class)
  ResponseEntity<ErrorResponse> handlerQuantityEatException(QuantityEatException e) {
    return ResponseEntity.status(e.getError().getStatusCode())
        .body((new ErrorResponse(Instant.now().toEpochMilli(),
            e.getError().getCode(),
            e.getError().getStatusCode(),
            e.getMessage(), new ArrayList<>())));
  }

}