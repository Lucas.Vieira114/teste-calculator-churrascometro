package br.com.calculator.api.testcalculator.repositories;

import br.com.calculator.api.testcalculator.entities.PersonEatsEntity;
import org.springframework.data.repository.CrudRepository;

public interface PersonEatsRepository extends CrudRepository<PersonEatsEntity, Long> { }