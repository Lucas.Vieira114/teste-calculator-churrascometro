package br.com.calculator.api.testcalculator.services.impl;

import br.com.calculator.api.testcalculator.entities.QuantityEatEntity;
import br.com.calculator.api.testcalculator.exceptions.QuantityEatException;
import br.com.calculator.api.testcalculator.exceptions.enums.QuantityEatExceptionEnum;
import br.com.calculator.api.testcalculator.repositories.QuantityEatRepository;
import br.com.calculator.api.testcalculator.services.QuantityEatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class QuantityEatServiceImpl implements QuantityEatService {

  @Autowired
  private QuantityEatRepository quantityEatRepository;

  @Override
  public List<QuantityEatEntity> findAll() {
    log.info("listing all datas on quantity-eat");
    List<QuantityEatEntity> quantityEatEntities = new ArrayList<>();
    for (QuantityEatEntity quantityEatEntity : quantityEatRepository.findAll()) {
      quantityEatEntities.add(quantityEatEntity);
    }
    return quantityEatEntities;
  }

  @Override
  public QuantityEatEntity findById(Long id) {
    checkIfQuantityEatExist(id);
    log.info("consulting quantity-eat by id: {}", id);
    return quantityEatRepository.findById(id).orElse(new QuantityEatEntity());
  }

  private void checkIfQuantityEatExist(Long id) {
    if (!quantityEatRepository.existsById(id)) {
      throw new QuantityEatException(QuantityEatExceptionEnum.QUANTITY_EAT_EXCEPTION_ENUM);
    }
  }

}