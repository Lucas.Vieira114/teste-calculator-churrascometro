package br.com.calculator.api.testcalculator.controllers;

import br.com.calculator.api.testcalculator.dtos.responses.PercentageBarbecueResponseDTO;
import br.com.calculator.api.testcalculator.facades.PercentageBarbecueFacade;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/queries/percentage-barbecue")
public class PercentageBarbecueController {

  @Autowired
  private PercentageBarbecueFacade percentageBarbecueFacade;

  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Porcentagens de churrascos listadas com sucesso")
  })
  @GetMapping
  public List<PercentageBarbecueResponseDTO> findAll() {
    return percentageBarbecueFacade.findAll();
  }

  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Porcentagem de churrasco listado com sucesso"),
      @ApiResponse(code = 400, message = "Porcentagem de churrasco nao encontrado")
  })
  @GetMapping("/{id}")
  public PercentageBarbecueResponseDTO findById(Long id) {
    return percentageBarbecueFacade.findById(id);
  }
}
