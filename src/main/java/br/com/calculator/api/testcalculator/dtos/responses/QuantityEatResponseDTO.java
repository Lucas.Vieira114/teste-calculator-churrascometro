package br.com.calculator.api.testcalculator.dtos.responses;

import br.com.calculator.api.testcalculator.enums.QuantityEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuantityEatResponseDTO {

  private Long id;
  private QuantityEnum quantityEnum;
  private Double percentage;

}