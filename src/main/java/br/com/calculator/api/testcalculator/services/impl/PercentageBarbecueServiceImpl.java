package br.com.calculator.api.testcalculator.services.impl;

import br.com.calculator.api.testcalculator.entities.PercentageBarbecueEntity;
import br.com.calculator.api.testcalculator.exceptions.PercentageBarbecueException;
import br.com.calculator.api.testcalculator.exceptions.enums.PercentageBarbecueExceptionEnum;
import br.com.calculator.api.testcalculator.repositories.PercentageBarbecueRepository;
import br.com.calculator.api.testcalculator.services.PercentageBarbecueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PercentageBarbecueServiceImpl implements PercentageBarbecueService {

  @Autowired
  private PercentageBarbecueRepository percentageBarbecueRepository;

  @Override
  public List<PercentageBarbecueEntity> findAll() {
    log.info("listing all datas on porcentage-barbecue");
    List<PercentageBarbecueEntity> percentageBarbecueEntities = new ArrayList<>();
    for (PercentageBarbecueEntity percentageBarbecueEntity : percentageBarbecueRepository.findAll()) {
      percentageBarbecueEntities.add(percentageBarbecueEntity);
    }
    return percentageBarbecueEntities;
  }

  @Override
  public PercentageBarbecueEntity findById(Long id) {
    checkIfPercentageBarbecueExist(id);
    log.info("consulting porcentage-barbecue by id: {}", id);
    return percentageBarbecueRepository.findById(id).orElse(new PercentageBarbecueEntity());
  }

  private void checkIfPercentageBarbecueExist(Long id) {
    if (!percentageBarbecueRepository.existsById(id)) {
      throw new PercentageBarbecueException(PercentageBarbecueExceptionEnum.PERCENTAGE_BARBECUE_EXCEPTION_ENUM);
    }
  }

}