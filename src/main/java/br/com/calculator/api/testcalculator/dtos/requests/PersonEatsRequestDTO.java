package br.com.calculator.api.testcalculator.dtos.requests;

import br.com.calculator.api.testcalculator.enums.PersonEatsEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonEatsRequestDTO {

    private String id;
    private PersonEatsEnum personEatsEnum;
    private Double amount;

}