package br.com.calculator.api.testcalculator.services.impl;

import br.com.calculator.api.testcalculator.entities.PersonEatsEntity;
import br.com.calculator.api.testcalculator.exceptions.PersonEatsException;
import br.com.calculator.api.testcalculator.exceptions.enums.PersonEatsExceptionEnum;
import br.com.calculator.api.testcalculator.repositories.PersonEatsRepository;
import br.com.calculator.api.testcalculator.services.PersonEatsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PersonEatsServiceImpl implements PersonEatsService {

  @Autowired
  private PersonEatsRepository personEatsRepository;

  @Override
  public List<PersonEatsEntity> findAll() {
    log.info("listing all datas on person-eats");
    List<PersonEatsEntity> personEatsEntities = new ArrayList<>();
    for (PersonEatsEntity personEatsEntity : personEatsRepository.findAll()) {
        personEatsEntities.add(personEatsEntity);
    }
    return personEatsEntities;
  }

  @Override
  public PersonEatsEntity findById(Long id) {
    checkIfPersonEatsExist(id);
    log.info("consulting person-eats by id: {}", id);
    return personEatsRepository.findById(id).orElse(new PersonEatsEntity());
  }

  private void checkIfPersonEatsExist(Long id) {
    if (!personEatsRepository.existsById(id)) {
      throw new PersonEatsException(PersonEatsExceptionEnum.PERSON_EATS_EXCEPTION_ENUM);
    }
  }

}