package br.com.calculator.api.testcalculator.exceptions;

import br.com.calculator.api.testcalculator.exceptions.enums.PercentageBarbecueExceptionEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class PercentageBarbecueException extends CalculatorApiException {
  private static final long serialVersionIOD = 4589179341768493322L;
  private final PercentageBarbecueExceptionEnum error;

  public PercentageBarbecueException(PercentageBarbecueExceptionEnum error) {
    super(error.getMessage());
    this.error = error;
  }
}