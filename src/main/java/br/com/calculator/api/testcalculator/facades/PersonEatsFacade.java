package br.com.calculator.api.testcalculator.facades;

import br.com.calculator.api.testcalculator.dtos.responses.PersonEatsResponseDTO;

import java.util.List;

public interface PersonEatsFacade {

    PersonEatsResponseDTO findById(Long id);
    List<PersonEatsResponseDTO> findAll();

}